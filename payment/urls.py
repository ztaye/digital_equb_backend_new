from django.urls import path, include
from rest_framework import routers
from .views import PaymentViewSet

router = routers.DefaultRouter()
router.register("pay", PaymentViewSet)

urlpatterns = [
    path("payment/", include(router.urls))
]
