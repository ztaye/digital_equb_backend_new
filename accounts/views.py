from rest_framework.views import APIView
from rest_framework.response import Response
from .models import PhoneOtp, User, BookAccount
from rest_framework import status, permissions, generics, viewsets
from django.shortcuts import get_object_or_404
# from .serializers import UserSerializer, LoginSerializer, RegisterUserSerializer, ProfileSerializer
from .serializers import ProfileSerializer, LoginSerializer, UserSerializer, BookAccountSerializer
from knox.views import LoginView as KnoxLoginView
from knox.models import AuthToken
# from knox.auth import TokenAuthentication
from rest_framework import filters
from django.contrib.auth import login
import random
from rest_framework.decorators import action
import datetime


def account_number_generator():
    account_number = random.randint(999999999, 9999999999)
    return account_number


class BookAccountListView(generics.ListCreateAPIView):
    queryset = BookAccount.objects.all()
    serializer_class = BookAccountSerializer

    def post(self, request, *args, **kwargs):
        user_phone = request.data.get('phone_number')
        balance = request.data.get('balance')

        # check if the account number already exist
        while True:
            account_number = account_number_generator()
            user_account = BookAccount.objects.filter(
                account_number__exact=account_number)

            if user_account.exists():
                continue

            else:
                break
        # serializer = BookAccountSerializer(data={
        #     "phone_number": user_phone,
        #     "balance": balance,
        #     "account_number": account_number
        # })

        book_account_obj1 = BookAccount.objects.create(phone_number=user_phone,
                                                       balance=balance,
                                                       account_number=account_number)
        book_account_obj1.save()

        return Response({
            "status": True,
            "id": book_account_obj1.id,
            "phone": book_account_obj1.phone_number,
            "account_number": book_account_obj1.account_number,
            "balance": book_account_obj1.balance
        })

        # if serializer.is_valid():
        #     serializer.save()
        #
        #     book_account_obj = BookAccount.objects.get(phone_number__exact=user_phone)
        #     return Response({
        #         "status": True,
        #         "id": book_account_obj.id,
        #         "phone": book_account_obj.phone_number,
        #         "account_number": book_account_obj.account_number,
        #        "balance": book_account_obj.balance
        #     })
        # else:
        #     return Response({
        #         "status": False,
        #         "data": "Error while creating "
        #     })


class BookAccountDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = BookAccount.objects.all()
    serializer_class = BookAccountSerializer


class ValidateUser(APIView):
    def post(self, request, *args, **kwargs):

        phone_sent = request.data.get("phone_number", False)
        if phone_sent:
            phone_temp1 = str(phone_sent)
            phone_temp2 = PhoneOtp.objects.filter(phone_number__iexact=phone_temp1)
            if phone_temp2.exists():
                return Response({
                    'status': False,
                    'detail': 'Phone number already exist',
                })
            else:
                key = otp_generator(phone_temp1)
                str_key = str(key)
                PhoneOtp.objects.create(
                    phone_number=phone_temp1,
                    otp=str_key,
                    count=1
                )

                if key:
                    # requests.request("GET", "https://2factor.in/API/V1/d674a078-7b91-11e9-ade6-0200cd936042/SMS/"+phone_temp1+"/"+str_key)

                    return Response({
                        'status': True,
                        'detail': key
                    })
                else:
                    return Response({
                        'status': False,
                        'detail': 'error in generating otp'
                    })
        else:
            return Response({
                'status': False,
                'detail': 'Phone number is not received from the form'
            })


def otp_generator(phone):
    if phone:
        key = random.randint(99999, 999999)
        return key
    else:
        return False


class ValidateOtp(APIView):

    def post(self, request, *args, **kwargs):

        phone_sent = request.data.get('phone_number', False)
        otp_sent = request.data.get('otp', False)

        if phone_sent and otp_sent:
            phone_temp1 = str(phone_sent)
            obj1 = PhoneOtp.objects.filter(phone_number__iexact=phone_temp1)

            if obj1.exists():
                obj2 = obj1.first()

                if str(obj2.otp) == str(otp_sent):
                    obj2.validated = True
                    obj2.save()
                    return Response({
                        'status': True,
                        'detail': 'phone confirmed successfully!'
                    })

                else:
                    return Response({
                        'status': False,
                        'err': 'INCORRECT OTP!'
                    })

            else:
                return Response({
                    'status': False,
                    'err': 'Phone number does not exist'
                })

        return Response({
            'status': False,
            'err': 'Phone or otp does not received!'
        })


class RegisterUserAPIViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = ProfileSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name', 'email', 'phone_number')

    def create(self, request):
        """ This validates and saves the registered regular user
         in the database. """

        serializer = ProfileSerializer(data=request.data)
        queryset = User.objects.all()

        serializer.is_valid(raise_exception=True)
        user = serializer.save()

        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })

        # if serializer.is_valid():
        #     user = serializer.save()
        #
        #     return Response({
        #         "user": UserSerializer(user, context=self.get_serializer_context()).data,
        #         "token": AuthToken.objects.create(user)[1]
        #     })
        # else:
        #     return Response(serializer.errors,
        #                     status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['get'], detail=False)
    def get_current_datetime(self, request):
        datetime_object = datetime.datetime.now()
        return Response({
            "data": datetime_object,
            "status": True
        })

    @action(detail=False, methods=['post'])
    def check_manual_credential(self, request):
        # check weather the user has passed the legal process

        global legal_check

        received_phone = request.data.get('phone_number')
        received_account = request.data.get('account_number')
        book_account_obj = BookAccount.objects.filter(phone_number__exact=received_phone,
                                                      account_number__exact=received_account)
        if book_account_obj.exists():
            # check weather the user already active
            book_account_obj2 = BookAccount.objects.filter(phone_number__exact=received_phone,
                                                           account_number__exact=received_account,
                                                           owner__phone_number__exact=received_phone)
            if book_account_obj2.exists():

                return Response({
                    "status": False,
                    "err": "the user is already active!"
                })
            else:
                legal_check = True

                return Response({
                    "status": True,
                    "data": legal_check,
                })
        else:
            return Response({
                "status": False,
                "err": "phone number and/or account number does not registered!"
            })


class LoginApi(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        # pwd = User.objects.filter(password__iexact=str(request.data.get('password')))
        serializer = LoginSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super().post(request, format=None)


class UserViewDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserViewListView(generics.ListCreateAPIView):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = User.objects.all()
    serializer_class = UserSerializer
