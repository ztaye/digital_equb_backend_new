from django.shortcuts import render
from rest_framework import viewsets, permissions
from join.serializers import GroupBookAccountSerializer
from join.models import GroupBookAccount, MonthCategory, Group, PayWin, MembersData
from accounts.models import BookAccount
from rest_framework.decorators import action
from rest_framework.response import Response
from sale_buy.models import SaleBuy
from datetime import datetime
from django.conf import settings
import random
import stripe

stripe_pub = settings.STRIPE_PUBLIC_KEY
stripe_private = settings.STRIPE_PRIVATE_KEY
stripe.api_key = stripe_private


def lottery_drawer(lots):
    if lots:
        lucky = random.choice(lots)
        return lucky
    else:
        return None


class PaymentViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = GroupBookAccount.objects.all()
    serializer_class = GroupBookAccountSerializer

    @action(methods=['post'], detail=False)
    def manual(self, request):
        # category
        # user_id
        # group_id

        global user_id, group_id, category_id, cat_amount, user_balance, season, cat_duration, admin_balance, admin_book_account_obj, pay_percent, payout_percent, user_money, admin_money, group_obj
        user_id = request.data.get('user_id')
        group_id = request.data.get('group_id')
        category_id = request.data.get('cat_id')

        cat_obj = MonthCategory.objects.get(id=category_id)
        cat_amount = cat_obj.cat_amount.amount
        cat_duration = cat_obj.cat_duration.duration

        user_book_obj = BookAccount.objects.get(owner_id__exact=user_id)
        user_balance = user_book_obj.balance

        admin_book_account_obj = BookAccount.objects.get(owner_id__exact=1)
        admin_balance = admin_book_account_obj.balance

        group_book_obj = GroupBookAccount.objects.get(group_id=group_id)

        group_balance = group_book_obj.balance

        group_obj = Group.objects.get(id=group_id)
        season = group_obj.season
        pay_percent = group_obj.pay_percent
        payout_percent = group_obj.payout_percent
        group_period = group_obj.period
        # group_variation = group_obj.variation
        activation_datetime = str(group_obj.activation_time.activated_at)

        activation_year = activation_datetime[0:4]
        activation_month = activation_datetime[5:7]
        activation_day = activation_datetime[8:10]

        total_activation_date_time = int(activation_year) * 365 + int(activation_month) * 30 + int(activation_day)

        current_datetime = datetime.now()  # this use to check if the time for the payment is reached

        current_year = current_datetime.strftime("%Y")
        current_month = current_datetime.strftime("%m")
        current_day = current_datetime.strftime("%d")
        total_current_datetime = int(current_year) * 365 + int(current_month) * 30 + int(current_day)

        payment_condition = (total_current_datetime - total_activation_date_time) % group_period

        if user_balance >= cat_amount:

            # # if the number of participants are odd
            # if cat_duration % 2 != 0:
            #     if season < (cat_duration / 2):
            #         user_money = group_balance

            user_book_obj.balance = user_balance - cat_amount
            group_book_obj.balance = group_balance + cat_amount
            group_balance = group_balance + cat_amount

            member_obj = MembersData.objects.get(group_id__exact=group_id, member_id__exact=user_id)
            member_id = member_obj.id
            pay_win_obj = PayWin.objects.create(member_id=member_id, season=season + 1, is_payed=True)

            pay_win_obj.save()
            user_book_obj.save()
            group_book_obj.save()

            pay_win_obj5 = PayWin.objects.filter(member__group=group_id, season=season + 1, is_payed=True,
                                                 is_winner=False).values_list('id', flat=True)
            pay_win_list = list(pay_win_obj5)
            # this block of code checks weather the payment day is reached or not

            if payment_condition == 0 and len(pay_win_list) == cat_duration:
            # if payment_condition == 0 and len(pay_win_list) == cat_duration and (
            #         total_activation_date_time != total_current_datetime):
                season = season + 1

                # pay_win_obj2 = PayWin.objects.filter(member__group=group_id,
                #                                      is_payed=True, is_winner=False). \
                #     values_list('id', flat=True)
                winners = PayWin.objects.filter(member__group=group_id, is_winner=True, is_payed=True).order_by(
                    'season'). \
                    values_list('member__member_id', flat=True)
                winners_list = list(winners)
                pay_win_obj2 = PayWin.objects.filter(member__group=group_id, is_winner=False, is_payed=True).exclude(
                    member__member_id__in=winners_list). \
                    values_list('member__member_id', flat=True).distinct()
                lucky = lottery_drawer(pay_win_obj2)

                if lucky:
                    pay_win_obj4 = PayWin.objects.get(member__member_id__exact=lucky, season=season, member__group_id__exact=group_id)
                    pay_win_obj4.is_winner = True
                    pay_win_obj4.save()

                    # if the number of participants are odd
                    if cat_duration % 2 != 0:
                        if season < (cat_duration / 2):
                            user_money = group_balance - (group_balance * pay_percent * 0.01)
                            admin_money = group_balance * pay_percent * 0.01
                            group_obj.pay_percent = group_obj.pay_percent - 1.0

                        if season == ((cat_duration // 2) + 1):
                            user_money = group_balance
                            admin_money = 0
                        if season != ((cat_duration // 2) + 1) and season > (cat_duration / 2):
                            user_money = group_balance + (group_balance * payout_percent * 0.01)
                            admin_money = 0 - group_balance * payout_percent * 0.01
                            group_obj.payout_percent = group_obj.payout_percent + 1.0

                    else:

                        if season < (cat_duration / 2):
                            user_money = group_balance - (group_balance * pay_percent * 0.01)
                            admin_money = group_balance * pay_percent * 0.01
                            group_obj.pay_percent = group_obj.pay_percent - 1.0

                        if season == (cat_duration / 2) or season == ((cat_duration / 2) + 1):
                            user_money = group_balance
                            admin_money = 0
                        if season > ((cat_duration / 2) + 1):
                            user_money = group_balance + (group_balance * payout_percent * 0.01)
                            admin_money = 0 - group_balance * payout_percent * 0.01
                            group_obj.payout_percent = group_obj.payout_percent - 1.0

                    # adding money to users account
                    user_book_account_obj2 = BookAccount.objects.get(
                        owner_id__exact=pay_win_obj4.member.member.id)
                    user_book_account_obj2.balance = user_book_account_obj2.balance + user_money
                    user_book_account_obj2.save()

                    # adding money to admins account
                    admin_book_account_obj.balance = admin_book_account_obj.balance + admin_money
                    admin_book_account_obj.save()

                    # taking money from group accounts
                    group_book_obj2 = GroupBookAccount.objects.get(group_id=group_id)
                    group_book_obj2.balance = group_book_obj2.balance - group_book_obj2.balance
                    group_book_obj2.save()

                    # checking if a user had borrowed
                    member_obj2 = MembersData.objects.get(member_id__exact=user_id, group_id__exact=group_id)
                    sale_buy_obj1 = SaleBuy.objects.filter(buyer_id__exact=member_obj2.id, group_id=group_id)
                    if sale_buy_obj1.exists():
                        sale_buy_obj2 = SaleBuy.objects.get(buyer_id__exact=member_obj2.id, group_id=group_id)
                        pre_payment = sale_buy_obj2.purchase_amount - user_money
                        real_payment = sale_buy_obj2.pre_payment - pre_payment
                        user_book_obj3 = BookAccount.objects.get(owner_id__exact=user_id)
                        user_book_obj3.balance = user_book_obj3 + real_payment
                        user_book_obj3.save()

                        # adding money to seller account
                        seller_membership_id = sale_buy_obj2.buyer_id
                        member_obj3 = MembersData.objects.get(id=seller_membership_id)
                        user_book_obj4 = BookAccount.objects.get(owner_id__exact=member_obj3.member_id)
                        purchase_amount = sale_buy_obj2.purchase_amount
                        user_book_obj4.balance = user_book_obj4.balance + (
                                    purchase_amount - (purchase_amount * 5 * 0.01))
                        user_book_obj4.save()

                        # adding money to admin account
                        admin_book_account_obj2 = BookAccount.objects.get(owner_id__exact=1)
                        admin_book_account_obj2.balance = admin_book_account_obj2.balance + purchase_amount * 5 * 0.01
                        
                    if season == cat_duration:
                        group_obj.is_active = False
                        group_obj.is_closed = True

                    group_obj.season = group_obj.season + 1
                    group_obj.save()

                    return Response({
                        "status": True,
                        "data": "Successfully paid!"
                    })

                else:
                    return Response({
                        "status": False,
                        "data": "error while generating lottery"
                    })
            return Response({
                "status": True,
                "data": "Successfully paid!"
            })

        else:

            return Response({
                "status": False,
                "data": "You have insufficient balance!"
            })

    @action(methods=['get'], detail=False)
    def online(self, request):

        key = stripe_pub

        return Response({
            'pub_key': key
        })

    @action(methods=['post'], detail=False)
    def charge(self, request):

        amount = request.data.get('amount')
        email = request.data.get('email')
        source = request.data.get('source')

        customer = stripe.Customer.create(
            email=email,
            source=source

        )
        charge = stripe.Charge.create(
            customer=customer.id,
            amount=amount,
            currency="aud",
            description='Equb payment'
        )

        global user_id, group_id, category_id, cat_amount, user_balance, season, cat_duration, admin_balance, admin_book_account_obj, pay_percent, payout_percent, user_money, admin_money, group_obj

        user_id = request.data.get('user_id')
        group_id = request.data.get('group_id')
        category_id = request.data.get('cat_id')

        cat_obj = MonthCategory.objects.get(id=category_id)
        cat_amount = cat_obj.cat_amount.amount
        cat_duration = cat_obj.cat_duration.duration

        user_book_obj = BookAccount.objects.get(owner_id__exact=user_id)
        user_balance = user_book_obj.balance

        admin_book_account_obj = BookAccount.objects.get(owner_id__exact=1)
        admin_balance = admin_book_account_obj.balance

        group_book_obj = GroupBookAccount.objects.get(group_id=group_id)

        group_balance = group_book_obj.balance

        group_obj = Group.objects.get(id=group_id)
        season = group_obj.season
        pay_percent = group_obj.pay_percent
        payout_percent = group_obj.payout_percent
        group_period = group_obj.period
        # group_variation = group_obj.variation
        activation_datetime = str(group_obj.activation_time.activated_at)

        activation_year = activation_datetime[0:4]
        activation_month = activation_datetime[5:7]
        activation_day = activation_datetime[8:10]

        total_activation_date_time = int(activation_year) * 365 + int(activation_month) * 30 + int(activation_day)

        current_datetime = datetime.now()  # this use to check if the time for the payment is reached

        current_year = current_datetime.strftime("%Y")
        current_month = current_datetime.strftime("%m")
        current_day = current_datetime.strftime("%d")
        total_current_datetime = int(current_year) * 365 + int(current_month) * 30 + int(current_day)

        payment_condition = (total_current_datetime - total_activation_date_time) % group_period

        # if user_balance >= cat_amount:

            # # if the number of participants are odd
            # if cat_duration % 2 != 0:
            #     if season < (cat_duration / 2):
            #         user_money = group_balance

        # user_book_obj.balance = user_balance - cat_amount
        group_book_obj.balance = group_balance + cat_amount
        group_balance = group_balance + cat_amount

        member_obj = MembersData.objects.get(group_id__exact=group_id, member_id__exact=user_id)
        member_id = member_obj.id
        pay_win_obj = PayWin.objects.create(member_id=member_id, season=season + 1, is_payed=True)

        pay_win_obj.save()
        # user_book_obj.save()
        group_book_obj.save()

        pay_win_obj5 = PayWin.objects.filter(member__group=group_id, season=season + 1, is_payed=True,
                                             is_winner=False).values_list('id', flat=True)
        pay_win_list = list(pay_win_obj5)
        # this block of code checks weather the payment day is reached or not
        if payment_condition == 0 and len(pay_win_list) == cat_duration:
        # if payment_condition == 0 and len(pay_win_list) == cat_duration and (
        #         total_activation_date_time != total_current_datetime):
            season = season + 1

            # pay_win_obj2 = PayWin.objects.filter(member__group=group_id,
            #                                      is_payed=True, is_winner=False). \
            #     values_list('id', flat=True)
            winners = PayWin.objects.filter(member__group=group_id, is_winner=True, is_payed=True).order_by(
                'season'). \
                values_list('member__member_id', flat=True)
            winners_list = list(winners)
            pay_win_obj2 = PayWin.objects.filter(member__group=group_id, is_winner=False, is_payed=True).exclude(
                member__member_id__in=winners_list). \
                values_list('member__member_id', flat=True).distinct()
            lucky = lottery_drawer(pay_win_obj2)

            if lucky:
                pay_win_obj4 = PayWin.objects.get(member__member_id__exact=lucky, season=season, member__group_id__exact=group_id)
                pay_win_obj4.save()

                # if the number of participants are odd
                if cat_duration % 2 != 0:
                    if season < (cat_duration / 2):
                        user_money = group_balance - (group_balance * pay_percent * 0.01)
                        admin_money = group_balance * pay_percent * 0.01
                        group_obj.pay_percent = group_obj.pay_percent - 1.0

                    if season == ((cat_duration // 2) + 1):
                        user_money = group_balance
                        admin_money = 0
                    if season != ((cat_duration // 2) + 1) and season > (cat_duration / 2):
                        user_money = group_balance + (group_balance * payout_percent * 0.01)
                        admin_money = 0 - group_balance * payout_percent * 0.01
                        group_obj.payout_percent = group_obj.payout_percent + 1.0

                else:

                    if season < (cat_duration / 2):
                        user_money = group_balance - (group_balance * pay_percent * 0.01)
                        admin_money = group_balance * pay_percent * 0.01
                        group_obj.pay_percent = group_obj.pay_percent - 1.0

                    if season == (cat_duration / 2) or season == ((cat_duration / 2) + 1):
                        user_money = group_balance
                        admin_money = 0
                    if season > ((cat_duration / 2) + 1):
                        user_money = group_balance + (group_balance * payout_percent * 0.01)
                        admin_money = 0 - group_balance * payout_percent * 0.01
                        group_obj.payout_percent = group_obj.payout_percent + 1.0

                # adding money to users account
                user_book_account_obj2 = BookAccount.objects.get(
                    owner_id__exact=pay_win_obj4.member.member.id)
                user_book_account_obj2.balance = user_book_account_obj2.balance + user_money
                user_book_account_obj2.save()

                # adding money to admins account
                admin_book_account_obj.balance = admin_book_account_obj.balance + admin_money
                admin_book_account_obj.save()

                # taking money from group accounts
                group_book_obj2 = GroupBookAccount.objects.get(group_id=group_id)
                group_book_obj2.balance = group_book_obj2.balance - group_book_obj2.balance
                group_book_obj2.save()

                # checking if a user had borrowed
                member_obj2 = MembersData.objects.get(member_id__exact=user_id, group_id__exact=group_id)
                sale_buy_obj1 = SaleBuy.objects.filter(buyer_id__exact=member_obj2.id, group_id=group_id)
                if sale_buy_obj1.exists():
                    sale_buy_obj2 = SaleBuy.objects.get(buyer_id__exact=member_obj2.id, group_id=group_id)
                    pre_payment = sale_buy_obj2.purchase_amount - user_money
                    real_payment = sale_buy_obj2.pre_payment - pre_payment
                    user_book_obj3 = BookAccount.objects.get(owner_id__exact=user_id)
                    user_book_obj3.balance = user_book_obj3 + real_payment
                    user_book_obj3.save()

                    # adding money to seller account
                    seller_membership_id = sale_buy_obj2.buyer_id
                    member_obj3 = MembersData.objects.get(id=seller_membership_id)
                    user_book_obj4 = BookAccount.objects.get(owner_id__exact=member_obj3.member_id)
                    purchase_amount = sale_buy_obj2.purchase_amount
                    user_book_obj4.balance = user_book_obj4.balance + (purchase_amount - (purchase_amount * 5 * 0.01))
                    user_book_obj4.save()

                    # adding money to admin account
                    admin_book_account_obj2 = BookAccount.objects.get(owner_id__exact=1)
                    admin_book_account_obj2.balance = admin_book_account_obj2.balance + purchase_amount * 5 * 0.01
                if season == cat_duration:
                    group_obj.is_active = False

                group_obj.season = group_obj.season + 1
                group_obj.save()

                return Response({
                    "status": True,
                    "data": "Successfully paid!"
                })

            else:
                return Response({
                    "status": False,
                    "data": "error while generating lottery"
                })
        return Response({
            "status": True,
            "data": "Successfully paid!"
        })

        # return Response({
        #     'status': True
        # })

    @action(methods=['post'], detail=False)
    def category(self, request):

        cat_id = request.data.get('cat_id')
        cat_obj = MonthCategory.objects.get(id=cat_id)

        cat_amounts = cat_obj.cat_amount.amount

        return Response({
            "amount": cat_amounts
        })
