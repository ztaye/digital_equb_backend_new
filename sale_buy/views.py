from rest_framework import viewsets
from .models import SaleBuy
from join.models import MonthCategory, MembersData, PayWin
from .serializers import SaleBuySerializer
from accounts.models import BookAccount
from rest_framework.response import Response
from rest_framework.decorators import action
from django.conf import settings
import stripe

stripe_pub = settings.STRIPE_PUBLIC_KEY
stripe_private = settings.STRIPE_PRIVATE_KEY
stripe.api_key = stripe_private


class SaleBuyViewSet(viewsets.ModelViewSet):
    queryset = SaleBuy.objects.all()
    serializer_class = SaleBuySerializer

    def create(self, request, *args, **kwargs):
        user_id = request.data.get('user_id')
        group_id = request.data.get('group_id')
        cat_id = request.data.get('cat_id')
        purchase_amount = request.data.get('purchase_amount')
        season = request.data.get('season')

        cat_obj = MonthCategory.objects.get(id=cat_id)
        cat_amount = cat_obj.cat_amount.amount
        cat_duration = cat_obj.cat_duration.duration

        user_book_obj = BookAccount.objects.get(owner_id__exact=user_id)

        if user_book_obj.balance < (cat_duration * cat_amount):
            return Response({
                "err": "insufficient balance"
            })

        # below block of code setts value for the original_amount
        global original_amount

        # if the number of participants are odd
        if cat_duration % 2 != 0:

            if season < (cat_duration / 2):
                original_amount = (cat_amount * cat_duration) - ((cat_amount * cat_duration) * (
                            10 - (season - 1)) * 0.01)  # the reason of season - 1 is  the first winner should pay 10%

            if season == ((cat_duration // 2) + 1):
                original_amount = cat_amount * cat_duration

            if season != ((cat_duration // 2) + 1) and season > (cat_duration / 2):
                if season == ((cat_duration // 2) + 2):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 1 * 0.01)
                if season == ((cat_duration // 2) + 3):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 2 * 0.01)
                if season == ((cat_duration // 2) + 4):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 3 * 0.01)
                if season == ((cat_duration // 2) + 5):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 4 * 0.01)
                if season == ((cat_duration // 2) + 6):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 5 * 0.01)
        else:

            if season < (cat_duration / 2):
                original_amount = (cat_amount * cat_duration) - ((cat_amount * cat_duration) * (
                            10 - (season - 1)) * 0.01)  # the reason of season - 1 is  the first winner should pay 10%

            if season == (cat_duration / 2) or season == ((cat_duration / 2) + 1):
                original_amount = cat_amount * cat_duration

            if season > ((cat_duration / 2) + 1):
                if season == ((cat_duration / 2) + 2):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 1 * 0.01)
                if season == ((cat_duration / 2) + 3):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 2 * 0.01)
                if season == ((cat_duration / 2) + 4):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 3 * 0.01)
                if season == ((cat_duration / 2) + 5):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 4 * 0.01)
                if season == ((cat_duration / 2) + 6):
                    original_amount = (cat_amount * cat_duration) + ((cat_amount * cat_duration) * 5 * 0.01)

        group_member_obj = MembersData.objects.get(group_id__exact=group_id, member_id__exact=user_id)

        serializer = SaleBuySerializer(data={
            "group": group_id,
            "seller": group_member_obj.id,
            "purchase_amount": purchase_amount,
            "original_amount": original_amount,
            "season": season,

        })

        if serializer.is_valid(raise_exception=True):
            user_book_obj.balance = user_book_obj.balance - original_amount
            user_book_obj.save()
            serializer.save()
            PayWin.objects.filter(member_id__exact=group_member_obj.id, member__group_id__exact=group_id, season=season, is_payed=True, is_winner=True).update(is_sale=True)
            return Response({
                "status": True,
                "data": "successfully purchased!"
            })

    # @action(methods=['post'], detail=False)
    # def buy(self, request):
    #
    #     buy_sale_id = request.data.get('id')
    #     user_id = request.data.get('user_id')
    #
    #     member_obj = MembersData.objects.get(member_id__exact=user_id)

    @action(methods=['post'], detail=False)
    def pre_payment(self, request):

        global pre_payment
        sale_buy_id = request.data.get('sale_buy_id')

        sale_buy_obj = SaleBuy.objects.get(id=sale_buy_id)
        cat_duration = sale_buy_obj.group.category.cat_duration.duration
        cat_amount = sale_buy_obj.group.category.cat_amount.amount

        if cat_duration % 2 != 0:
            # this calculate the worst case
            pre_payment = sale_buy_obj.purchase_amount - (
                        (10 - (cat_duration // 2) - 1) * 0.01 * cat_duration * cat_amount) - (cat_duration * cat_amount)
        else:
            pre_payment = sale_buy_obj.purchase_amount - (
                        (10 - (cat_duration / 2) - 2) * 0.01 * cat_duration * cat_amount) - (cat_duration * cat_amount)

        return Response({
            "data": pre_payment
        })

    @action(methods=['post'], detail=False)
    def pay_manual(self, request):

        sale_buy_id = request.data.get('sale_buy_id')
        pre_payments = request.data.get('pre_payment')
        user_id = request.data.get('user_id')

        sale_buy_obj = SaleBuy.objects.get(id=sale_buy_id)
        user_book_obj = BookAccount.objects.get(owner_id__exact=user_id)

        user_book_obj.balance = user_book_obj.balance + sale_buy_obj.original_amount - pre_payments
        user_book_obj.save()

        members_obj = MembersData.objects.get(member_id__exact=user_id, group_id__exact=sale_buy_obj.group.id)
        SaleBuy.objects.filter(id=sale_buy_id).update(buyer=members_obj.id, pre_payment=pre_payments, is_sold=True)

        return Response({
            "status": True,
            "data": "Successfully paid!"
        })

    @action(methods=['post'], detail=False)
    def pay_online(self, request):

        amount = request.data.get('pre_payment')
        email = request.data.get('email')
        source = request.data.get('source')

        customer = stripe.Customer.create(
            email=email,
            source=source

        )
        charge = stripe.Charge.create(
            customer=customer.id,
            amount=amount,
            currency="aud",
            description='Equb payment'
        )

        sale_buy_id = request.data.get('sale_buy_id')
        pre_payments = request.data.get('pre_payment')
        user_id = request.data.get('user_id')

        sale_buy_obj = SaleBuy.objects.get(id=sale_buy_id)
        user_book_obj = BookAccount.objects.get(owner_id__exact=user_id)

        user_book_obj.balance = user_book_obj.balance + sale_buy_obj.original_amount
        user_book_obj.save()

        members_obj = MembersData.objects.get(member_id__exact=user_id, group_id__exact=sale_buy_obj.group.id)
        SaleBuy.objects.filter(id=sale_buy_id).update(buyer=members_obj.id, pre_payment=pre_payments, is_sold=True)

        return Response({
            "status": True,
            "data": "Successfully paid!"
        })



