from rest_framework import serializers
from .models import Status, MonthCategory, Activities, FirstPayment, Group, MembersData, MonthDurations, Periods, \
    Amounts, PayWin, GroupBookAccount, GroupActivationDateTime
from sale_buy.serializers import SaleBuySerializer
from django.conf import settings
from rest_framework.response import Response
from accounts.models import User


# class PeriodSerializer(serializers.Serializer):
#     class Meta:
#         model = Periods
#         fields = '__all__'
#
#
# class DurationSerializer(serializers.Serializer):
#     class Meta:
#         model = MonthDurations
#         fields = '__all__'
#
#
# class AmountSerializer(serializers.Serializer):
#     class Meta:
#         model = Amounts
#         fields = '__all__'


class GroupActivationDateTimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupActivationDateTime
        fields = '__all__'


class GroupBookAccountSerializer(serializers.Serializer):
    class Meta:
        model = GroupBookAccount
        fields = '__all__'


class PayWinSerializer(serializers.ModelSerializer):
    class Meta:
        model = PayWin
        fields = '__all__'


# class SeasonsSerializer(serializers.ModelSerializer):
#     seasons_detail = PayWinSerializer(many=False, read_only=True)
#
#     class Meta:
#         model = Seasons
#         fields = ('id', 'members_data', 'seasons_detail')


class MonthDurationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = MonthDurations
        fields = '__all__'


class PeriodsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Periods
        fields = '__all__'


class AmountsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Amounts
        fields = '__all__'


class MembersDataSerializer(serializers.ModelSerializer):
    # seasons = SeasonsSerializer(many=False, read_only=True)
    seasons = PayWinSerializer(many=True, read_only=True)

    class Meta:
        model = MembersData
        fields = ('id', 'group', 'member', 'seasons')


class GroupSerializer(serializers.ModelSerializer):
    groups_data = MembersDataSerializer(many=True, read_only=True)
    book = GroupBookAccountSerializer(many=False, read_only=True)
    activation_time = GroupActivationDateTimeSerializer(many=False, read_only=True)
    sale_buy = SaleBuySerializer(many=True, read_only=True)

    class Meta:
        model = Group
        fields = ('id', 'group_name', 'category', 'members', 'season', 'groups_data', 'is_active', 'is_closed',
                  'period', 'sale_buy', 'book', 'activation_time')


class FirstPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = FirstPayment
        fields = ('id', 'status_owner', 'amount')

        def create(self, validated_data):
            first_payment_obj = FirstPayment.objects.create(status_owner_id=validated_data['status_owner'],
                                                            amount=validated_data['amount'])
            first_payment_obj.save()

            return first_payment_obj


class ActivitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activities
        fields = '__all__'


class StatusSerializer(serializers.ModelSerializer):
    first_payment = FirstPaymentSerializer(many=True, read_only=True)

    class Meta:
        model = Status
        fields = ('id', 'owner', 'category', 'is_active', 'is_pending', 'first_payment')

        def create(self, validated_data):
            status = Status.objects.create(owner=validated_data['owner'], category=validated_data['category'])
            status.save()
            return status


class MonthCategorySerializer(serializers.ModelSerializer):
    status_categories = StatusSerializer(many=True, read_only=True)
    activities_categories = ActivitiesSerializer(many=True, read_only=True)

    class Meta:
        model = MonthCategory
        fields = ('id', 'cat_name', 'cat_period', 'cat_duration', 'cat_amount', 'status_categories', 'activities_categories')


class GroupMembersSerializer(serializers.ModelSerializer):
    user_groups = GroupSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'phone_number', 'user_groups')

    # def get_group_members(self, validated_data):
    #
    #     members = User.objects.get(user_groups.id=validated_data['group_id'])
