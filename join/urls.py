from django.urls import path, include
from rest_framework import routers
from .views import StatusViewSet, ActivitiesViewSet, FirstPaymentViewSet, MonthCategoryViewSet, PeriodsViewSet,\
    MonthDurationsViewSet, AmountsViewSet, GroupViewSet, GroupMembersViewSet

router = routers.DefaultRouter()
router.register('status', StatusViewSet)
router.register('activities', ActivitiesViewSet)
router.register('first_payment', FirstPaymentViewSet)
router.register('category', MonthCategoryViewSet)
router.register('periods', PeriodsViewSet)
router.register('month_durations', MonthDurationsViewSet)
router.register('amounts', AmountsViewSet)
router.register('groups', GroupViewSet)
router.register('group/members', GroupMembersViewSet)


urlpatterns = [
    path('auth/', include(router.urls))
]
