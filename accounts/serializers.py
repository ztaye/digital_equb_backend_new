from rest_framework import serializers
from .models import User, BookAccount
from django.contrib.auth import authenticate
from join.serializers import StatusSerializer, GroupSerializer, MembersDataSerializer
from pay_lot.serializers import EqubSerializer
from pay_lot.models import EqubBook


class BookAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookAccount
        fields = ('id', 'phone_number', 'balance', 'account_number')

    # def post(self, validated_data):
    #     book_account_obj = BookAccount.objects.create(phone_number=validated_data['phone_number'],
    #                                                   balance=validated_data['balance'],
    #                                                   account_number=validated_data['account_number'])
    #     book_account_obj.save()
    #
    #     return book_account_obj


class ProfileSerializer(serializers.ModelSerializer):
    """ A serializer for our user profiles """
    book = BookAccountSerializer(many=False, read_only=False)
    id = serializers.ReadOnlyField()
    is_active = serializers.ReadOnlyField()
    password = serializers.CharField(max_length=255,
                                     style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ['id', 'is_active', 'first_name',
                  'last_name', 'email', 'password', 'book']

        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data, **extra_fields):

        book = validated_data.pop('book')

        # Check weather the user passed the manual procedure
        check_user_book = BookAccount.objects.filter(phone_number__exact=book['phone_number'],
                                                     account_number__exact=book['account_number']).\
            values_list('phone_number', 'id')
        if check_user_book.exists():
            """ Creates and returns a new user """

            # Validating Data
            user = User.objects.create_user(phone_number=check_user_book[0][0],
                                            password=validated_data['password'],
                                            email=validated_data['email'],
                                            first_name=validated_data['first_name'],
                                            last_name=validated_data['last_name'],
                                            )
            print(book)
            book_account_obj = BookAccount.objects.get(id=check_user_book[0][1])
            book_account_obj.owner = user
            book_account_obj.save()
            # user = {"data": True}
            user.save()

            return user
        else:
            msg = {
                "status": False,
                "data": "The phone or the account number does not exist"
            }

            raise serializers.ValidationError(msg, code='authorization')


class LoginSerializer(serializers.Serializer):
    phone_number = serializers.CharField()
    password = serializers.CharField(style={'input_type': 'password'}, trim_whitespace=False)

    def authenticate(self, **kwargs):
        return authenticate(self.context['request'], **kwargs)

    def validate(self, data):
        phone_number = data.get('phone_number')
        password = data.get('password')

        if phone_number and password:
            obj1 = User.objects.filter(phone_number=phone_number)
            if obj1.exists():

                # user = authenticate(phone_number=phone_number, password=password)

                user = self.authenticate(phone_number=phone_number, password=password)
            # user = authenticate(request=self.context.get("request"), phone_number=phone, password=password)

            else:
                msg = {
                    'status': False,
                    'detail': 'Phone number does not exist'
                }
                raise serializers.ValidationError(msg, code='authorization')
            if not user:
                msg = {
                    'status': False,
                    'detail': 'Phone number and password does not match, try again!'
                }
                raise serializers.ValidationError(msg, code='authorization')

        else:
            msg = {
                'status': False,
                'detail': 'Phone or password don not received',
            }
            raise serializers.ValidationError(msg, code='authorization')
        data['user'] = user
        return data


class UserSerializer(serializers.ModelSerializer):
    statuses = StatusSerializer(many=True, read_only=True)
    user_groups = GroupSerializer(many=True, read_only=True)
    members_group_data = MembersDataSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'phone_number', 'first_name', 'last_name', 'email', 'is_staff', 'is_active', 'avatar',
                  'date_joined', 'statuses', 'user_groups', 'members_group_data')
