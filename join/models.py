from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.timezone import utc, now
import datetime


class Periods(models.Model):
    period = models.CharField(max_length=255, unique=True, blank=True, null=True)

    def __str__(self):
        return str(self.period)


class MonthDurations(models.Model):
    duration = models.IntegerField(unique=True, blank=True, null=True)

    def __str__(self):
        return str(self.duration)


class Amounts(models.Model):
    amount = models.IntegerField(unique=True, blank=True, null=True)

    def __str__(self):
        return str(self.amount)


class MonthCategory(models.Model):
    cat_name = models.CharField(max_length=255, unique=True, blank=True, null=True)
    cat_period = models.ForeignKey(Periods, related_name="periods", on_delete=models.CASCADE, null=True, blank=True)
    cat_duration = models.ForeignKey(MonthDurations, related_name="durations", on_delete=models.CASCADE, null=True,
                                     blank=True)
    cat_amount = models.ForeignKey(Amounts, related_name="amounts", on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.cat_name


class Status(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="statuses", on_delete=models.CASCADE, blank=True,
                              null=True)
    category = models.ForeignKey(MonthCategory, related_name="status_categories", on_delete=models.CASCADE, blank=True,
                                 null=True)
    is_active = models.BooleanField(default=False)
    is_pending = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)


class Activities(models.Model):
    category = models.ForeignKey(MonthCategory, related_name="activities_categories", on_delete=models.CASCADE,
                                 blank=True,
                                 null=True)
    counter = models.IntegerField(default=0)
    group_counter = models.IntegerField(default=0)

    def __str__(self):
        return str(self.category.cat_name)


class Group(models.Model):
    group_name = models.CharField(max_length=255, blank=True, null=True)
    members = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="user_groups", through="MembersData",
                                     blank=True)
    category = models.ForeignKey(MonthCategory, related_name="members_cat_data", on_delete=models.CASCADE, blank=True,
                                 null=True)
    season = models.IntegerField(default=0)
    is_active = models.BooleanField(default=False)
    is_closed = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)
    # this field tracks weather the equb is daily, weekly and monthly
    period = models.PositiveSmallIntegerField(null=True, blank=True)
    # this field created for the favour of day, only 1 on days category
    # variation = models.PositiveSmallIntegerField(null=True, blank=True)
    pay_percent = models.FloatField(null=True, blank=True, default=10.0)
    payout_percent = models.FloatField(null=True, blank=True, default=1.0)
    # pay_percent = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    # payout_percent = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)

    def __str__(self):
        return str(self.id)


class GroupActivationDateTime(models.Model):
    activated_at = models.DateTimeField(auto_now_add=True)
    group = models.OneToOneField(Group, on_delete=models.CASCADE, related_name="activation_time", blank=True,
                                 null=True)


class GroupBookAccount(models.Model):
    group = models.OneToOneField(Group, on_delete=models.CASCADE, null=True, blank=True, related_name="book")
    account_number = models.BigIntegerField(null=True, blank=True)
    balance = models.FloatField(null=True, blank=True, default=0.0)


class MembersData(models.Model):
    group = models.ForeignKey(Group, related_name="groups_data", null=True, blank=True, on_delete=models.CASCADE)
    member = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="members_group_data", on_delete=models.CASCADE,
                               blank=True, null=True)
    # category = models.ForeignKey(MonthCategory, related_name="members_cat_data", on_delete=models.CASCADE, blank=True,
    #                              null=True)
    season = models.IntegerField(default=0)

    def __str__(self):
        return str(self.id)


class FirstPayment(models.Model):
    status_owner = models.ForeignKey(Status, on_delete=models.CASCADE, related_name="first_payment")
    # is_payed = models.BooleanField(default=False)
    amount = models.IntegerField()

    def __unicode__(self):
        return self.status_owner.category.cat_name


class PayWin(models.Model):
    # seasons_model = models.ForeignKey(Seasons, on_delete=models.CASCADE, related_name="seasons_detail", blank=True,
    #                                   null=True)
    member = models.ForeignKey(MembersData, on_delete=models.CASCADE, related_name="seasons",
                               blank=True,
                               null=True)
    season = models.IntegerField(blank=True, null=True)
    is_payed = models.BooleanField(default=False)
    is_winner = models.BooleanField(default=False)
    is_sale = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)

# class GroupActivity(models.Model):
#     group_id = models.ForeignKey(Group, on_delete=models.CASCADE, null=True, blank=True, related_name="group_activity")
#     season = models.PositiveIntegerField(null=True, blank=True)
#     winner = models.ForeignKey()
