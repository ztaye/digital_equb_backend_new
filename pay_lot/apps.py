from django.apps import AppConfig


class PayLotConfig(AppConfig):
    name = 'pay_lot'
