from rest_framework import serializers
from .models import SaleBuy


class SaleBuySerializer(serializers.ModelSerializer):
    class Meta:
        model = SaleBuy
        fields = '__all__'

    def create(self, validated_data):

        sale_buy_obj = SaleBuy.objects.create(group=validated_data['group'], seller=validated_data['seller'],
                                              purchase_amount=validated_data['purchase_amount'],
                                              original_amount=validated_data['original_amount'],
                                              season=validated_data['season'])

        sale_buy_obj.save()
        return sale_buy_obj
