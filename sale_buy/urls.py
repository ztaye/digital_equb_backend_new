from django.urls import path, include
from rest_framework import routers
from .views import SaleBuyViewSet
router = routers.DefaultRouter()
router.register("main", SaleBuyViewSet)
urlpatterns = [
    path("sale_buy/", include(router.urls))
]


