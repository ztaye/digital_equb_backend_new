from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.core.validators import RegexValidator

from .managers import UserManager


# --------------------------USER DATABASE------------------------------------
class User(AbstractBaseUser, PermissionsMixin):
    phone_regex = RegexValidator(regex=r'^\+?2?\d{10,15}$',
                                 message="Phone number must be entered in the format '+251927702700'. "
                                         "Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=15, unique=True)
    email = models.EmailField(_('email address'), max_length=255, blank=True, null=True)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    # is_superuser = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to='resources/pic/', default='resources/pic/download.png', blank=True, null=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.phone_number


# --------------------------PHONE OTP DATABASE------------------------------------

class PhoneOtp(models.Model):
    phone_regex = RegexValidator(regex=r'^\+?2?\d{10,15}$',
                                 message="Phone number must be entered in the format '+251927702700'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=15, unique=True, null=False)
    otp = models.PositiveIntegerField(null=True, blank=True)
    count = models.IntegerField(default=0, help_text='Number of Otp sent')
    validated = models.BooleanField(default=False,
                                    help_text='This is the tracker for validating and confirmation of otp')

    def __str__(self):
        return self.otp


class BookAccount(models.Model):
    phone_regex = RegexValidator(regex=r'^\+?2?\d{10,15}$',
                                 message="Phone number must be entered in the format '+251927702700'. "
                                         "Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=15, unique=False)
    owner = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True, related_name="book")
    balance = models.FloatField(null=True, blank=True, default=0.0)
    account_number = models.BigIntegerField(null=True, blank=True)
