# Generated by Django 2.1.7 on 2019-07-07 21:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sale_buy', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='salebuy',
            name='season',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
