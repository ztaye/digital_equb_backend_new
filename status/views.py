from rest_framework.viewsets import ModelViewSet
from .serializers import FeedBackSerializer
from .models import FeedBack
from join.models import PayWin
from join.serializers import PayWinSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import permissions
from accounts.models import BookAccount


class FeedBackViewSet(ModelViewSet):
    serializer_class = FeedBackSerializer
    queryset = FeedBack.objects.all()
    permission_classes = [permissions.IsAdminUser]


class CheckLotSell(ModelViewSet):
    queryset = PayWin.objects.all()
    serializer_class = PayWinSerializer

    @action(methods=['post'], detail=False)
    def get_sell_lots(self, request):
        user_id = request.data.get('id')

        sale_array = PayWin.objects.filter(member__member_id__exact=user_id, is_winner=True, is_payed=True).values_list('is_sale', flat=True)

        sale_array_list = list(sale_array)

        return Response({
            "status": True,
            "data": sale_array_list
        })

    @action(methods=['post'], detail=False)
    def balance(self, request):
        phone_number = request.data.get('phone_number')
        book_obj = BookAccount.objects.get(phone_number__exact=phone_number)

        balance = book_obj.balance

        return Response({
            "balance": balance
        })
