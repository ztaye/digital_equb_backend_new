from django.db import models


# Create your models here.
class FeedBack(models.Model):
    contact = models.CharField(max_length=256, null=True, blank=True)
    feedback = models.TextField(max_length=1000, null=True, blank=True)
