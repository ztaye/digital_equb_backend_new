from django.urls import path, include
from rest_framework import routers
from .views import FeedBackViewSet, CheckLotSell

route = routers.DefaultRouter()

route.register("feedback", FeedBackViewSet)
route.register("sale", CheckLotSell)

urlpatterns = [
    path("status/", include(route.urls))
]
