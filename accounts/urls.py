from django.urls import path, include
from . import views
from knox import views as knox_views
from rest_framework import routers

router = routers.SimpleRouter()
router.register('', views.RegisterUserAPIViewSet)


urlpatterns = [
    path('register/', views.ValidateUser.as_view(), name='user_registration'),
    path('register/confirm/', views.ValidateOtp.as_view(), name='otp_confirmation'),
    path('register/form/', include(router.urls)),
    path('login/', views.LoginApi.as_view()),
    path('logout/', knox_views.LogoutView.as_view()),
    path('book_account/', views.BookAccountListView.as_view()),
    path('book_account/<int:pk>/', views.BookAccountDetailView.as_view()),
    path('user/<int:pk>/', views.UserViewDetailView.as_view()),
    path('user/', views.UserViewListView.as_view())
]
