from rest_framework import serializers
from .models import EqubBook


class EqubSerializer(serializers.ModelSerializer):
    class Meta:
        model = EqubBook
        fields = ('id', 'balance', 'account_number')
