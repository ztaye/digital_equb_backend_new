from django.db import models
from join.models import Group, MembersData


class SaleBuy(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE, blank=True, null=True, related_name="sale_buy")
    seller = models.ForeignKey(MembersData, on_delete=models.CASCADE, blank=True, null=True, related_name="seller")
    buyer = models.ForeignKey(MembersData, on_delete=models.CASCADE, blank=True, null=True, related_name="buyer")
    is_sold = models.BooleanField(default=False)
    purchase_amount = models.PositiveIntegerField(blank=True, null=True)
    original_amount = models.PositiveIntegerField(blank=True, null=True)
    season = models.PositiveSmallIntegerField(blank=True, null=True)
    pre_payment = models.FloatField(null=True, blank=True, default=0.0)
