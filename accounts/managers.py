from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):

    def create_user(self, phone_number, password, email=None, first_name=None, last_name=None, avatar=None, **extra_fields):

        if not phone_number:
            raise ValueError(_('The phone number must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email,
                          phone_number=phone_number,
                          first_name=first_name,
                          last_name=last_name,
                          avatar=avatar,

                          **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, phone_number, password, **extra_fields):

        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(phone_number, password, **extra_fields)
