from rest_framework.response import Response
from rest_framework import viewsets
from .models import Status, MonthCategory, Activities, FirstPayment, Group, MembersData, Periods, MonthDurations, \
    Amounts, PayWin, GroupBookAccount, GroupActivationDateTime
from .serializers import StatusSerializer, ActivitiesSerializer, FirstPaymentSerializer, MonthCategorySerializer, \
    MonthDurationsSerializer, PeriodsSerializer, AmountsSerializer, GroupSerializer, GroupMembersSerializer, \
    PayWinSerializer, GroupBookAccountSerializer
from accounts.models import User
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.decorators import action
from django.conf import settings
import random
from accounts.models import BookAccount
import stripe

stripe_pub = settings.STRIPE_PUBLIC_KEY
stripe_private = settings.STRIPE_PRIVATE_KEY
stripe.api_key = stripe_private


def lottery_drawer(lots):
    if lots:
        lucky = random.choice(lots)
        return lucky
    else:
        return None


def group_account_number_generator():
    account_number = random.randint(999999999, 9999999999)
    return account_number


class StatusViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    serializer_class = StatusSerializer
    queryset = Status.objects.all()

    def create(self, request, *args, **kwargs):
        # queryset = Status.objects.all()
        owner_temp1 = request.data.get("owner", False)
        cat_temp1 = request.data.get("category", False)

        if owner_temp1 and cat_temp1:
            owner_temp2 = User.objects.filter(phone_number__iexact=str(owner_temp1))
            cat_temp2 = MonthCategory.objects.filter(cat_name__iexact=str(cat_temp1))

            if cat_temp2.exists() and owner_temp2.exists():
                owner = User.objects.get(phone_number=str(owner_temp1))
                owner_id = owner.id
                cat = MonthCategory.objects.get(cat_name=str(cat_temp1))
                cat_id = cat.id

                serializer = StatusSerializer(data={
                    "owner": owner_id,
                    "category": cat_id
                })

                if serializer.is_valid(raise_exception=True):
                    serializer.save()

                    return Response({
                        "status": True,
                        "data": serializer.data
                    })
                else:
                    return Response({
                        "status": False,
                        "data": "Invalid data"
                    })
            else:
                return Response({
                    "status": False,
                    "data": "User doesn't exist"
                })

        else:
            return Response({
                "status": False,
                "data": "Values not provided"
            })


class ActivitiesViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = Activities.objects.all()
    serializer_class = ActivitiesSerializer


class FirstPaymentViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = FirstPayment.objects.all()
    serializer_class = FirstPaymentSerializer

    def create(self, request, *args, **kwargs):

        received_owner = request.data.get('status_owner')
        received_amount = int(request.data.get('amount'))

        first_payment_obj = FirstPayment.objects.filter(status_owner=received_owner)

        global group_id, check_new_group, group__account, status_obj, cat_amount, user_book_account_obj, cat_period, group_obj

        status_obj = Status.objects.get(id=received_owner)
        cat_amount = status_obj.category.cat_amount.amount
        cat_period = status_obj.category.cat_period.period
        # owner_obj = User.objects.get(id=status_obj.owner.id)
        user_book_account_obj = BookAccount.objects.get(owner_id__exact=status_obj.owner.id)

        # check if user have sufficient balance in his account
        if user_book_account_obj.balance < cat_amount:
            return Response({
                "status": False,
                "err": "you have insufficient balance!"
            })

        if first_payment_obj.exists():
            return Response({
                "status": False,
                "err": "A user with the same status has already exist"
            })

        else:

            # status_obj = Status.objects.get(id=received_owner)
            if status_obj:
                user = status_obj.owner.id
                # cat_amount = status_obj.category.cat_amount.amount
                cat_duration = status_obj.category.cat_duration.duration
                cat_id = status_obj.category.id
                activities_obj = Activities.objects.get(category_id__exact=cat_id)
                activities_counter = activities_obj.counter
                # global group_id, check_new_group, group__account

                if activities_counter < cat_duration:
                    if cat_amount == received_amount:

                        serializer = FirstPaymentSerializer(data=request.data)
                        if serializer.is_valid():
                            serializer.save()

                            counter = activities_counter + 1
                            activities_obj.counter = counter
                            activities_obj.save()

                            # check weather the user is new to the group, if so create new group
                            if counter == 1:
                                check_new_group = True

                                # check the group period type to set period  on the group table
                                if cat_period == 'day':
                                    group_obj = Group.objects.create(category_id=cat_id, period=1)
                                if cat_period == 'month':
                                    group_obj = Group.objects.create(category_id=cat_id, period=7)
                                if cat_period == 'year':
                                    group_obj = Group.objects.create(category_id=cat_id, period=30)

                                group_obj.save()

                                group_id = group_obj.id
                                members_data_obj = MembersData.objects.create(group_id=group_obj.id,
                                                                              member_id=user)
                                members_data_obj.save()

                                # check if the group account number already exist
                                while True:
                                    account_number = group_account_number_generator()
                                    group__account = GroupBookAccount.objects.filter(
                                        account_number__exact=account_number)

                                    if group__account.exists():
                                        continue

                                    else:
                                        break

                                pay_win_obj = PayWin.objects.create(member=members_data_obj, season=1, is_payed=True)
                                user_book_account_obj.balance = user_book_account_obj.balance - received_amount
                                user_book_account_obj.save()

                                group_book_account_obj = GroupBookAccount.objects.create(group=group_obj,
                                                                                         account_number=account_number,
                                                                                         balance=received_amount)
                                pay_win_obj.save()
                                group_book_account_obj.save()

                            else:
                                check_new_group = False
                                existing_group_obj = Group.objects.get(category_id__exact=cat_id, is_active=False,
                                                                       is_closed=False)
                                members_data_obj2 = MembersData.objects.create(group_id=existing_group_obj.id,
                                                                               member_id=user)
                                group_id = existing_group_obj.id

                                members_data_obj2.save()

                                pay_win_obj2 = PayWin.objects.create(member=members_data_obj2, season=1, is_payed=True)
                                pay_win_obj2.save()

                                user_book_account_obj.balance = user_book_account_obj.balance - received_amount
                                user_book_account_obj.save()

                                group_book_account_obj2 = GroupBookAccount.objects.get(group=existing_group_obj)

                                group_book_account_obj2.balance = group_book_account_obj2.balance + received_amount
                                group_book_account_obj2.save()

                                if counter == cat_duration:

                                    pay_win_obj3 = PayWin.objects.filter(member__group=existing_group_obj.id,
                                                                         is_payed=True, is_winner=False, season=1). \
                                        values_list('id', flat=True)
                                    pay_win_obj3_list = list(pay_win_obj3)
                                    lucky = lottery_drawer(pay_win_obj3_list)

                                    if lucky:

                                        pay_win_obj4 = PayWin.objects.get(id=lucky)
                                        pay_win_obj4.is_winner = True
                                        pay_win_obj4.save()

                                        percent = existing_group_obj.pay_percent

                                        # adding money to the winner's account number
                                        group_book_account_obj3 = GroupBookAccount.objects.get(group_id=group_id)
                                        user_book_account_obj2 = BookAccount.objects.get(
                                            owner_id__exact=pay_win_obj4.member.member.id)

                                        group_money = group_book_account_obj3.balance
                                        user_money = group_money - (group_money * percent * 0.01)
                                        user_book_account_obj2.balance = user_money + user_book_account_obj2.balance
                                        user_book_account_obj2.save()

                                        # adding money to admins account
                                        admin_book_account_obj = BookAccount.objects.get(owner_id__exact=1)
                                        admin_money = group_money * percent * 0.01
                                        admin_book_account_obj.balance = admin_book_account_obj.balance + admin_money
                                        admin_book_account_obj.save()

                                        # taking money from the group account
                                        group_book_account_obj3.balance = group_book_account_obj3.balance - group_book_account_obj3.balance
                                        group_book_account_obj3.save()

                                        # decreasing pay_percent amount by 0.5

                                        existing_group_obj.pay_percent = existing_group_obj.pay_percent - 1.0
                                        existing_group_obj.is_active = True
                                        existing_group_obj.season = 1
                                        existing_group_obj.save()

                                        # setting the group activation time in to group activation date time model
                                        group_activation_datetime_obj = GroupActivationDateTime.objects.create(
                                            group=existing_group_obj)
                                        group_activation_datetime_obj.save()

                                        activities_obj2 = Activities.objects.get(category_id__exact=cat_id)
                                        activities_group_counter = activities_obj2.group_counter + 1
                                        activities_obj2.group_counter = activities_group_counter
                                        reset = 0
                                        activities_obj2.counter = reset
                                        activities_obj2.save()

                                    else:

                                        return Response({
                                            "status": False,
                                            "data": "error while generating a lottery"

                                        })

                            g_id = group_id
                            return Response({
                                "status": True,
                                "data": {
                                    "g_id": g_id,
                                    "check_new_group": check_new_group,
                                }

                            })
                        else:
                            return Response({
                                "status": False,
                                "data": "Invalid data"

                            })

                    else:
                        return Response({
                            "status": False,
                            "data": ("Amount should be equal to " + str(cat_amount) + " Birr")

                        })
                        # return Response({
                        #     "status": False,
                        #     "data": {
                        #         "data": ("Amount should be equal to " + str(cat_amount) + " Birr")
                        #     }
                        #
                        # })
                else:
                    return Response({
                        "status": False,
                        "data": "group has occupied"

                    })

            else:
                return Response({
                    "status": False,
                    "data": "user does not exist on status table"

                })

    @action(methods=['post'], detail=False)
    def online(self, request, *args, **kwargs):

        amount = request.data.get('amount')
        email = request.data.get('email')
        source = request.data.get('source')

        customer = stripe.Customer.create(
            email=email,
            source=source

        )
        charge = stripe.Charge.create(
            customer=customer.id,
            amount=amount,
            currency="aud",
            description='Equb payment'
        )
        received_owner = request.data.get('status_owner')
        received_amount = int(request.data.get('amount'))

        first_payment_obj = FirstPayment.objects.filter(status_owner=received_owner)

        global group_id, check_new_group, group__account, status_obj, cat_amount, user_book_account_obj, cat_period, group_obj

        status_obj = Status.objects.get(id=received_owner)
        cat_amount = status_obj.category.cat_amount.amount
        cat_period = status_obj.category.cat_period.period
        # owner_obj = User.objects.get(id=status_obj.owner.id)
        user_book_account_obj = BookAccount.objects.get(owner_id__exact=status_obj.owner.id)

        # check if user have sufficient balance in his account

        if first_payment_obj.exists():
            return Response({
                "status": False,
                "err": "A user with the same status has already exist"
            })

        else:

            # status_obj = Status.objects.get(id=received_owner)
            if status_obj:
                user = status_obj.owner.id
                # cat_amount = status_obj.category.cat_amount.amount
                cat_duration = status_obj.category.cat_duration.duration
                cat_id = status_obj.category.id
                activities_obj = Activities.objects.get(category_id__exact=cat_id)
                activities_counter = activities_obj.counter
                # global group_id, check_new_group, group__account

                if activities_counter < cat_duration:
                    if cat_amount == received_amount:

                        serializer = FirstPaymentSerializer(data=request.data)
                        if serializer.is_valid():
                            serializer.save()

                            counter = activities_counter + 1
                            activities_obj.counter = counter
                            activities_obj.save()

                            # check weather the user is new to the group, if so create new group
                            if counter == 1:
                                check_new_group = True

                                # check the group period type to set period  on the group table
                                if cat_period == 'day':
                                    group_obj = Group.objects.create(category_id=cat_id, period=1)
                                if cat_period == 'month':
                                    group_obj = Group.objects.create(category_id=cat_id, period=7)
                                if cat_period == 'year':
                                    group_obj = Group.objects.create(category_id=cat_id, period=30)

                                group_obj.save()

                                group_id = group_obj.id
                                members_data_obj = MembersData.objects.create(group_id=group_obj.id,
                                                                              member_id=user)
                                members_data_obj.save()

                                # check if the group account number already exist
                                while True:
                                    account_number = group_account_number_generator()
                                    group__account = GroupBookAccount.objects.filter(
                                        account_number__exact=account_number)

                                    if group__account.exists():
                                        continue

                                    else:
                                        break

                                pay_win_obj = PayWin.objects.create(member=members_data_obj, season=1, is_payed=True)
                                # user_book_account_obj.balance = user_book_account_obj.balance - received_amount
                                # user_book_account_obj.save()

                                group_book_account_obj = GroupBookAccount.objects.create(group=group_obj,
                                                                                         account_number=account_number,
                                                                                         balance=received_amount)
                                pay_win_obj.save()
                                group_book_account_obj.save()

                            else:
                                check_new_group = False
                                existing_group_obj = Group.objects.get(category_id__exact=cat_id, is_active=False,
                                                                       is_closed=False)
                                members_data_obj2 = MembersData.objects.create(group_id=existing_group_obj.id,
                                                                               member_id=user)
                                group_id = existing_group_obj.id

                                members_data_obj2.save()

                                pay_win_obj2 = PayWin.objects.create(member=members_data_obj2, season=1, is_payed=True)
                                pay_win_obj2.save()

                                user_book_account_obj.balance = user_book_account_obj.balance - received_amount
                                user_book_account_obj.save()

                                group_book_account_obj2 = GroupBookAccount.objects.get(group=existing_group_obj)

                                group_book_account_obj2.balance = group_book_account_obj2.balance + received_amount
                                group_book_account_obj2.save()

                                if counter == cat_duration:

                                    pay_win_obj3 = PayWin.objects.filter(member__group=existing_group_obj.id,
                                                                         is_payed=True, is_winner=False, season=1). \
                                        values_list('id', flat=True)
                                    lucky = lottery_drawer(pay_win_obj3)

                                    if lucky:

                                        pay_win_obj4 = PayWin.objects.get(id=lucky)
                                        pay_win_obj4.is_winner = True
                                        pay_win_obj4.save()

                                        percent = existing_group_obj.pay_percent

                                        # adding money to the winner's account number
                                        group_book_account_obj3 = GroupBookAccount.objects.get(group_id=group_id)
                                        user_book_account_obj2 = BookAccount.objects.get(
                                            owner_id__exact=pay_win_obj4.member.member.id)

                                        group_money = group_book_account_obj3.balance
                                        user_money = group_money - (group_money * percent * 0.01)
                                        user_book_account_obj2.balance = user_money + user_book_account_obj2.balance
                                        user_book_account_obj2.save()

                                        # adding money to admins account
                                        admin_book_account_obj = BookAccount.objects.get(owner_id__exact=1)
                                        admin_money = group_money * percent * 0.01
                                        admin_book_account_obj.balance = admin_book_account_obj.balance + admin_money
                                        admin_book_account_obj.save()

                                        # taking money from the group account
                                        group_book_account_obj3.balance = group_book_account_obj3.balance - group_book_account_obj3.balance
                                        group_book_account_obj3.save()

                                        # decreasing pay_percent amount by 0.5

                                        existing_group_obj.pay_percent = existing_group_obj.pay_percent - 1.0
                                        existing_group_obj.is_active = True
                                        existing_group_obj.season = 1
                                        existing_group_obj.save()

                                        # setting the group activation time in to group activation date time model
                                        group_activation_datetime_obj = GroupActivationDateTime.objects.create(
                                            group=existing_group_obj)
                                        group_activation_datetime_obj.save()

                                        activities_obj2 = Activities.objects.get(category_id__exact=cat_id)
                                        activities_group_counter = activities_obj2.group_counter + 1
                                        activities_obj2.group_counter = activities_group_counter
                                        reset = 0
                                        activities_obj2.counter = reset
                                        activities_obj2.save()

                                    else:

                                        return Response({
                                            "status": False,
                                            "data": "error while generating a lottery"

                                        })

                            g_id = group_id
                            return Response({
                                "status": True,
                                "data": {
                                    "g_id": g_id,
                                    "check_new_group": check_new_group,
                                }

                            })
                        else:
                            return Response({
                                "status": False,
                                "data": "Invalid data"

                            })

                    else:
                        return Response({
                            "status": False,
                            "data": ("Amount should be equal to " + str(cat_amount) + " Birr")

                        })
                        # return Response({
                        #     "status": False,
                        #     "data": {
                        #         "data": ("Amount should be equal to " + str(cat_amount) + " Birr")
                        #     }
                        #
                        # })
                else:
                    return Response({
                        "status": False,
                        "data": "group has occupied"

                    })

            else:
                return Response({
                    "status": False,
                    "data": "user does not exist on status table"

                })


class MonthCategoryViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = MonthCategory.objects.all()
    serializer_class = MonthCategorySerializer

    @action(detail=False, methods=['post'])
    def check_category(self, request):
        period = request.data.get('cat_period')
        month_duration = request.data.get('cat_duration')
        amount = request.data.get('cat_amount')

        month_category_check = MonthCategory.objects.filter(cat_amount__amount__iexact=amount,
                                                            cat_period__period__iexact=period,
                                                            cat_duration__duration__iexact=month_duration)
        if month_category_check.exists():
            month_category_obj = MonthCategory.objects.get(cat_amount__amount__iexact=amount,
                                                           cat_period__period__iexact=period,
                                                           cat_duration__duration__iexact=month_duration)
            cat_name = month_category_obj.cat_name
            return Response({
                "status": True,
                "data": cat_name
            })
        else:
            return Response({
                "status": False,
                "err": "There is no such category with provided parameters!"
            })

    @action(methods=['post'], detail=False)
    def create_cat(self, request):

        duration = request.data.get('duration')
        amount = request.data.get('amount')
        period = request.data.get('period')
        cat_name = request.data.get('cat_name')

        duration_obj = MonthDurations.objects.get(duration__exact=duration)
        amount_obj = Amounts.objects.get(amount__exact=amount)
        period_obj = Periods.objects.get(period__exact=period)

        cat_obj = MonthCategory.objects.create(cat_name=cat_name, cat_amount=amount_obj, cat_duration=duration_obj,
                                               cat_period=period_obj)
        cat_obj.save()

        activity_obj = Activities.objects.create(category=cat_obj)
        activity_obj.save()

        return Response({
            "status": True
        })


class PeriodsViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = Periods.objects.all()
    serializer_class = PeriodsSerializer


class MonthDurationsViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = MonthDurations.objects.all()
    serializer_class = MonthDurationsSerializer


class AmountsViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    queryset = Amounts.objects.all()
    serializer_class = AmountsSerializer


class GroupViewSet(viewsets.ModelViewSet):
    # permission_classes = [
    #     permissions.IsAuthenticated
    # ]
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    # this function gives an admin role in creating the group name for the first subscriber of the group
    @action(detail=False, methods=['post'])
    def set_group_name(self, request):
        received_id = request.data.get('group_id')
        received_name = request.data.get('group_name')
        group_obj = Group.objects.get(id=received_id)

        group_obj.group_name = received_name
        group_obj.save()

        return Response({
            "status": True,
            "data": received_name
        })

    # this function returns a members in the group
    @action(detail=False, methods=['post'])
    def get_group_members(self, request):
        g_id = request.data.get('group_id')
        members_id = MembersData.objects.filter(group_id=g_id).values_list('member', flat=True)
        # members = User.objects.filter(id_in=members_id).values_list('phone_number', flat=True)
        members = User.objects.filter(id__in=members_id).values_list('phone_number', flat=True)
        # members = group_obj.members

        return Response({
            "status": True,
            "data": members,
        })

    # this function returns winners in every season through array format
    @action(detail=False, methods=['post'])
    def get_winners(self, request):

        global winners_dict, group_id

        group_id = request.data.get('group_id')
        winners_dict = {}

        # check weather the request is for single group of not
        # if group_id.lenght == 1:
        #     winners = PayWin.objects.filter(member__group=group_id, is_winner=True, is_payed=True).order_by('season'). \
        #         values_list('member__member_id', flat=True)
        #     winners_list = list(winners)
        #     winners_phone = User.objects.filter(id__in=[winners]).values_list('id', 'phone_number')
        #     ordered_winners = [-1] * len(winners_list)
        #
        #     # preserving an order
        #     for x in winners_phone:
        #         ordered_winners[winners_list.index(x[0])] = x
        #
        #     # filtering only the phone numbers form 2D id and phone list
        #     ordered_winners_phone = [y for x, y in ordered_winners]
        #     return Response({
        #         "status": True,
        #         "data": ordered_winners_phone
        #     })

        for g_id in group_id:

            winners = PayWin.objects.filter(member__group=g_id, is_winner=True, is_payed=True).order_by('season'). \
                values_list('member__member_id', flat=True)
            winners_list = list(winners)
            winners_phone = User.objects.filter(id__in=[winners]).values_list('id', 'phone_number')
            ordered_winners = [-1] * len(winners_list)

            # preserving an order
            for x in winners_phone:
                ordered_winners[winners_list.index(x[0])] = x

            # filtering only the phone numbers form 2D id and phone list
            ordered_winners_phone = [y for x, y in ordered_winners]
            winners_dict[g_id] = ordered_winners_phone

        return Response({
            "status": True,
            "data": winners_dict
        })
        # g_id = request.data.get('group_id')
        # winners = PayWin.objects.filter(member__group=g_id, is_winner=True, is_payed=True).order_by('season'). \
        #     values_list('member__member_id', flat=True)
        # winners_list = list(winners)
        # winners_phone = User.objects.filter(id__in=[winners]).values_list('id', 'phone_number')
        # ordered_winners = [-1] * len(winners_list)
        #
        # # preserving an order
        # for x in winners_phone:
        #     ordered_winners[winners_list.index(x[0])] = x
        #
        # # filtering only the phone numbers form 2D id and phone list
        # ordered_winners_phone = [y for x, y in ordered_winners]
        # return Response({
        #     "status": True,
        #     "data": ordered_winners_phone
        # })


class GroupMembersViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = GroupMembersSerializer

    # def list(self, request, *args, **kwargs):
    # @action(detail=False, methods=['post'])
    # def get_group_members(self, request):
    #     serializer = GroupMembersSerializer(data=request.data)
    #
    #     if serializer.is_valid():
    #         serializer.save()
    #
    #         return Response({
    #             "status": True,
    #             "data": serializer.data
    #         })
    #     else:
    #         return Response({
    #             "status": False,
    #             "data": "serializer is not valid"
    #         })
