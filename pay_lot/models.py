from django.db import models
from django.conf import settings


class EqubBook(models.Model):
    owner = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True,
                                 related_name="equb_book")
    balance = models.IntegerField(null=True)
    account_number = models.BigIntegerField(null=True, blank=True)
