from django.apps import AppConfig


class SaleBuyConfig(AppConfig):
    name = 'sale_buy'
